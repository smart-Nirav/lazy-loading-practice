export class RouteConstant {
  //public routing
  public static LOGIN = 'login';

  //feature module route
  public static FEATURE_MODULE_ROUTE = 'feature';
  public static EMPLOYEE_MODULE_ROUTE = 'employee';
  public static COURSE_MODULE_ROUTE = 'course';
  public static BATCH_MODULE_ROUTE = 'batch';
  public static NOTIFICATION_MODULE_ROUTE = 'notification';

  //feature component route
  public static DASHBOARD_ROUTE='';
  public static DASHBOARD=`${RouteConstant.FEATURE_MODULE_ROUTE}/${RouteConstant.DASHBOARD_ROUTE}`;

  public static EMPLOYEE_LIST_ROUTE = '';
  public static EMPLOYEE_LIST = `${RouteConstant.FEATURE_MODULE_ROUTE}/${RouteConstant.EMPLOYEE_MODULE_ROUTE}/${RouteConstant.EMPLOYEE_LIST_ROUTE}`;

  public static EMPLOYEE_VIEW_ROUTE = 'view';
  public static EMPLOYEE_VIEW = `${RouteConstant.FEATURE_MODULE_ROUTE}/${RouteConstant.EMPLOYEE_MODULE_ROUTE}/${RouteConstant.EMPLOYEE_VIEW_ROUTE}`;

  public static EMPLOYEE_DETAIL_ROUTE = 'detail';
  public static EMPLOYEE_DETAIL = `${RouteConstant.FEATURE_MODULE_ROUTE}/${RouteConstant.EMPLOYEE_MODULE_ROUTE}/${RouteConstant.EMPLOYEE_DETAIL_ROUTE}`;

  public static COURSE_LIST_ROUTE='';
  public static COURSE_LIST=`${RouteConstant.FEATURE_MODULE_ROUTE}/${RouteConstant.COURSE_MODULE_ROUTE}/${RouteConstant.COURSE_LIST_ROUTE}`;

  public static COURSE_VIEW_ROUTE='view';
  public static COURSE_VIEW=`${RouteConstant.FEATURE_MODULE_ROUTE}/${RouteConstant.COURSE_MODULE_ROUTE}/${RouteConstant.COURSE_VIEW_ROUTE}`;

  public static COURSE_DETAIL_ROUTE='detail';
  public static COURSE_DETAIL=`${RouteConstant.FEATURE_MODULE_ROUTE}/${RouteConstant.COURSE_MODULE_ROUTE}/${RouteConstant.COURSE_DETAIL_ROUTE}`;

  public static BATCH_LIST_ROUTE='';
  public static BATCH_LIST=`${RouteConstant.FEATURE_MODULE_ROUTE}/${RouteConstant.BATCH_MODULE_ROUTE}/${RouteConstant.BATCH_LIST_ROUTE}`;

  public static BATCH_DETAIL_ROUTE='detail';
  public static BATCH_DETAIL=`${RouteConstant.FEATURE_MODULE_ROUTE}/${RouteConstant.BATCH_MODULE_ROUTE}/${RouteConstant.BATCH_DETAIL_ROUTE}`;

  public static BATCH_VIEW_ROUTE='view';
  public static BATCH_VIEW=`${RouteConstant.FEATURE_MODULE_ROUTE}/${RouteConstant.BATCH_MODULE_ROUTE}/${RouteConstant.BATCH_VIEW_ROUTE}`;

  public static NOTIFICATION_LIST_ROUTE='';
  public static NOTIFICATION_LIST=`${RouteConstant.FEATURE_MODULE_ROUTE}/${RouteConstant.NOTIFICATION_MODULE_ROUTE}/${RouteConstant.NOTIFICATION_LIST_ROUTE}`;

  public static NOTIFICATION_VIEW_ROUTE='view';
  public static NOTIFICATION_VIEW=`${RouteConstant.FEATURE_MODULE_ROUTE}/${RouteConstant.NOTIFICATION_MODULE_ROUTE}/${RouteConstant.NOTIFICATION_VIEW_ROUTE}`;

  public static NOTIFICATION_DETAIL_ROUTE='detail';
  public static NOTIFICATION_DETAIL=`${RouteConstant.FEATURE_MODULE_ROUTE}/${RouteConstant.NOTIFICATION_MODULE_ROUTE}/${RouteConstant.NOTIFICATION_DETAIL_ROUTE}`;

}
