import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';
import { RouteConstant } from '../../shared-module/route.constant';
import { NotificationListComponent } from './components/notification-list/notification-list.component';
import { NotificationDetailComponent } from './components/notification-detail/notification-detail.component';
import { NotificationViewComponent } from './components/notification-view/notification-view.component';

const routes: Routes = [
  {
    path:RouteConstant.NOTIFICATION_DETAIL_ROUTE,
    component:NotificationDetailComponent
  },
  {
    path:RouteConstant.NOTIFICATION_LIST_ROUTE,
    component:NotificationListComponent
  },
  {
    path:RouteConstant.NOTIFICATION_VIEW_ROUTE,
    component:NotificationViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotificationRoutingModule { }
