import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationRoutingModule } from './notification-routing.module';
import { NotificationListComponent } from './components/notification-list/notification-list.component';
import { NotificationDetailComponent } from './components/notification-detail/notification-detail.component';
import { NotificationViewComponent } from './components/notification-view/notification-view.component';


@NgModule({
  declarations: [NotificationListComponent, NotificationDetailComponent, NotificationViewComponent],
  imports: [
    CommonModule,
    NotificationRoutingModule
  ]
})
export class NotificationModule { 
  constructor(){
    console.log('notification module loaded');
  }
}
