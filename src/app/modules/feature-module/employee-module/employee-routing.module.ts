import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { from } from 'rxjs';
import { RouteConstant } from '../../shared-module/route.constant';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeViewComponent } from './employee-view/employee-view.component';
import { EmployeeDetailComponent } from './employee-detail/employee-detail.component';

const routes: Routes = [

    {
        path:RouteConstant.EMPLOYEE_LIST_ROUTE,
        component:EmployeeListComponent
    },
    {
        path:RouteConstant.EMPLOYEE_VIEW_ROUTE,
        component:EmployeeViewComponent
    },
    {
        path:RouteConstant.EMPLOYEE_DETAIL_ROUTE,
        component:EmployeeDetailComponent
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeRoutingModule { }
