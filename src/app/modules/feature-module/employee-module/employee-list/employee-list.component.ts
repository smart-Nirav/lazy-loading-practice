import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { RouteConstant } from '../../../shared-module/route.constant';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get viewUrl(){
    return '/' + RouteConstant.EMPLOYEE_VIEW;
  }

  get dashboardUrl(){
    return '/' + RouteConstant.DASHBOARD;
  }

}
