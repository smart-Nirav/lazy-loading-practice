import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { RouteConstant } from '../../../shared-module/route.constant';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.scss']
})
export class EmployeeDetailComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get viewUrl(){
    return '/' + RouteConstant.EMPLOYEE_VIEW;
  }
}
