import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatureRoutingModule } from './feature-routing.module';
import { from } from 'rxjs';
import { DashboardComponent } from './component/dashboard/dashboard.component';

@NgModule({
  declarations: [DashboardComponent],
  imports: [
    CommonModule,
    FeatureRoutingModule,
  ]
})
export class FeatureModule {
  constructor(){
    console.log('feature module loaded');
  }
 }
