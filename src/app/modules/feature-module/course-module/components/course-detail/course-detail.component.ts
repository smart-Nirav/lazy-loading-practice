import { Component, OnInit } from '@angular/core';
import { RouteConstant } from '../../../../shared-module/route.constant';

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.scss']
})
export class CourseDetailComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get viewUrl(){
    return '/' + RouteConstant.COURSE_VIEW;
  }
}
