import { Component, OnInit } from '@angular/core';
import { RouteConstant } from '../../../shared-module/route.constant';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get employeeUrl(){
    return '/' + RouteConstant.EMPLOYEE_LIST;
  }

  get courseUrl(){
    return '/' + RouteConstant.COURSE_LIST;
  }

  get batchUrl(){
    return '/' + RouteConstant.BATCH_LIST;
  }

  get notificationUrl(){
    return '/' + RouteConstant.NOTIFICATION_LIST;
  }

}
