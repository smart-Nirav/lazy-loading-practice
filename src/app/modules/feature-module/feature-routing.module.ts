import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteConstant } from '../shared-module/route.constant';
import { DashboardComponent } from './component/dashboard/dashboard.component';

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: RouteConstant.EMPLOYEE_MODULE_ROUTE,
        loadChildren: () =>
          import('./employee-module/employee.module').then(
            (m) => m.EmployeeModule
          ),
      },
      {
        path: RouteConstant.COURSE_MODULE_ROUTE,
        loadChildren: () =>
          import('./course-module/course.module').then(
            (m) => m.CourseModule
          ),
      },
      {
        path:RouteConstant.DASHBOARD_ROUTE,
        component:DashboardComponent
      }
      ,
      {
        path: RouteConstant.BATCH_MODULE_ROUTE,
        loadChildren: () =>
          import('./batch-module/batch.module').then(
            (m) => m.BatchModule
          ),
      },
      {
        path: RouteConstant.NOTIFICATION_MODULE_ROUTE,
        loadChildren: () =>
          import('./notification-module/notification.module').then(
            (m) => m.NotificationModule
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeatureRoutingModule {}
