import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RouteConstant } from '../../shared-module/route.constant';
import { BatchListComponent } from './components/batch-list/batch-list.component';
import { BatchDetailComponent } from './components/batch-detail/batch-detail.component';
import { BatchViewComponent } from './components/batch-view/batch-view.component';
import { from } from 'rxjs';

const routes: Routes = [
  {
    path:RouteConstant.BATCH_LIST_ROUTE,
    component:BatchListComponent
  },
  {
    path:RouteConstant.BATCH_DETAIL_ROUTE,
    component:BatchDetailComponent
  },
  {
    path:RouteConstant.BATCH_VIEW_ROUTE,
    component:BatchViewComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BatchRoutingModule { }