import { Component, OnInit } from '@angular/core';
import { from } from 'rxjs';
import { RouteConstant } from '../../../../shared-module/route.constant';

@Component({
  selector: 'app-batch-list',
  templateUrl: './batch-list.component.html',
  styleUrls: ['./batch-list.component.scss']
})
export class BatchListComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get dashboardUrl(){
    return '/' + RouteConstant.DASHBOARD;
  }

  get viewUrl(){
    return '/' + RouteConstant.BATCH_VIEW;
  }

}
