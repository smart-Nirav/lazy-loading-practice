import { Component, OnInit } from '@angular/core';
import { RouteConstant } from '../../../shared-module/route.constant';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  get employeeUrl(){
    return '/' + RouteConstant.DASHBOARD;
  }

}
