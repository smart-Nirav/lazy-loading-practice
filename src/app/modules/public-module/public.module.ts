import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { PublicRoutingModule } from '../public-module/public-routing.module';

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    PublicRoutingModule
  ]
})
export class PublicModule { 
  constructor(){
    console.log('public module loaded');
  }
}
